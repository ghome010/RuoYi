package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.CallRecord;

/**
 * 【请填写功能名称】Mapper接口
 * 
 * @author ruoyi
 * @date 2021-04-20
 */
public interface CallRecordMapper 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    public CallRecord selectCallRecordById(Integer id);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param callRecord 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<CallRecord> selectCallRecordList(CallRecord callRecord);

    /**
     * 新增【请填写功能名称】
     * 
     * @param callRecord 【请填写功能名称】
     * @return 结果
     */
    public int insertCallRecord(CallRecord callRecord);

    /**
     * 修改【请填写功能名称】
     * 
     * @param callRecord 【请填写功能名称】
     * @return 结果
     */
    public int updateCallRecord(CallRecord callRecord);

    /**
     * 删除【请填写功能名称】
     * 
     * @param id 【请填写功能名称】ID
     * @return 结果
     */
    public int deleteCallRecordById(Integer id);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteCallRecordByIds(String[] ids);
}
