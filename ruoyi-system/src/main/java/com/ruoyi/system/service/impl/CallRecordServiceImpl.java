package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.CallRecordMapper;
import com.ruoyi.system.domain.CallRecord;
import com.ruoyi.system.service.ICallRecordService;
import com.ruoyi.common.core.text.Convert;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-04-20
 */
@Service
public class CallRecordServiceImpl implements ICallRecordService 
{
    @Autowired
    private CallRecordMapper callRecordMapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    @Override
    public CallRecord selectCallRecordById(Integer id)
    {
        return callRecordMapper.selectCallRecordById(id);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param callRecord 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<CallRecord> selectCallRecordList(CallRecord callRecord)
    {
        return callRecordMapper.selectCallRecordList(callRecord);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param callRecord 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertCallRecord(CallRecord callRecord)
    {
        return callRecordMapper.insertCallRecord(callRecord);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param callRecord 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateCallRecord(CallRecord callRecord)
    {
        return callRecordMapper.updateCallRecord(callRecord);
    }

    /**
     * 删除【请填写功能名称】对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteCallRecordByIds(String ids)
    {
        return callRecordMapper.deleteCallRecordByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】ID
     * @return 结果
     */
    @Override
    public int deleteCallRecordById(Integer id)
    {
        return callRecordMapper.deleteCallRecordById(id);
    }
}
