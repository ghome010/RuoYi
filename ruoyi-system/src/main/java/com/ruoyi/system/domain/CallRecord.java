package com.ruoyi.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 【请填写功能名称】对象 call_record
 * 
 * @author ruoyi
 * @date 2021-04-20
 */
public class CallRecord extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Integer id;

    /** 虚拟号编号 */
    @Excel(name = "虚拟号编号")
    private String virNum;

    /** 坐席用户ID */
    @Excel(name = "坐席用户ID")
    private String userId;

    /** 0未拨打 1拨打未接通 2无效号码 3接通无意向 4有意向 */
    @Excel(name = "0未拨打 1拨打未接通 2无效号码 3接通无意向 4有意向")
    private Integer status;

    /** 客户联系方式 加密存储 */
    @Excel(name = "客户联系方式 加密存储")
    private String cusPhone;

    /** 所在区 */
    @Excel(name = "所在区")
    private String districtName;

    /** 车品牌 */
    @Excel(name = "车品牌")
    private String brandName;

    /** 车颜色 */
    @Excel(name = "车颜色")
    private String autoColor;

    /** 车状态是否挂售 */
    @Excel(name = "车状态是否挂售")
    private String autoStatus;

    /** 通话记录 */
    @Excel(name = "通话记录")
    private String callRecords;

    /** $column.columnComment */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "通话记录", width = 30, dateFormat = "yyyy-MM-dd")
    private Date gmtCreated;

    /** $column.columnComment */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "通话记录", width = 30, dateFormat = "yyyy-MM-dd")
    private Date gmtModified;

    /** 0正常 1删除  */
    @Excel(name = "0正常 1删除 ")
    private Integer optStatus;

    /** 车龄 */
    @Excel(name = "车龄")
    private Integer autoAge;

    /** 加个区间 */
    @Excel(name = "加个区间")
    private String autoPriceRange;

    /** 车型 */
    @Excel(name = "车型")
    private String autoType;

    /** 车牌归属省份 */
    @Excel(name = "车牌归属省份")
    private String autoPlateProv;

    /** 车牌归属市 */
    @Excel(name = "车牌归属市")
    private String autoPlateDist;

    /** 看车城市 */
    @Excel(name = "看车城市")
    private String autoSeeCity;

    /** 看车区域 */
    @Excel(name = "看车区域")
    private String autoSeeDistrict;

    /** 看车具体街道信息 */
    @Excel(name = "看车具体街道信息")
    private String autoSeeStreet;

    /** 车辆上牌时间 */
    @Excel(name = "车辆上牌时间")
    private String autoPlateTime;

    /** 过户次数 */
    @Excel(name = "过户次数")
    private Integer autoTransferNum;

    /** 是否着急出售0否 1是 */
    @Excel(name = "是否着急出售0否 1是")
    private Integer autoExcitStatus;

    /** 客户姓名 */
    @Excel(name = "客户姓名")
    private String cusName;

    /** m男 w女 */
    @Excel(name = "m男 w女")
    private String cusSex;

    /** 车辆里程 */
    @Excel(name = "车辆里程")
    private String autoMileage;

    /** 课程省份 */
    @Excel(name = "课程省份")
    private String autoSeeProv;

    /** 是否推送成功0未成功 1成功 默认 */
    @Excel(name = "是否推送成功0未成功 1成功 默认")
    private Integer pushStatus;

    /** 延迟推送时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "延迟推送时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date pushTime;

    /** 0立刻推送 1延迟推送 */
    @Excel(name = "0立刻推送 1延迟推送")
    private Integer pushType;

    public void setId(Integer id) 
    {
        this.id = id;
    }

    public Integer getId() 
    {
        return id;
    }
    public void setVirNum(String virNum) 
    {
        this.virNum = virNum;
    }

    public String getVirNum() 
    {
        return virNum;
    }
    public void setUserId(String userId) 
    {
        this.userId = userId;
    }

    public String getUserId() 
    {
        return userId;
    }
    public void setStatus(Integer status) 
    {
        this.status = status;
    }

    public Integer getStatus() 
    {
        return status;
    }
    public void setCusPhone(String cusPhone) 
    {
        this.cusPhone = cusPhone;
    }

    public String getCusPhone() 
    {
        return cusPhone;
    }
    public void setDistrictName(String districtName) 
    {
        this.districtName = districtName;
    }

    public String getDistrictName() 
    {
        return districtName;
    }
    public void setBrandName(String brandName) 
    {
        this.brandName = brandName;
    }

    public String getBrandName() 
    {
        return brandName;
    }
    public void setAutoColor(String autoColor) 
    {
        this.autoColor = autoColor;
    }

    public String getAutoColor() 
    {
        return autoColor;
    }
    public void setAutoStatus(String autoStatus) 
    {
        this.autoStatus = autoStatus;
    }

    public String getAutoStatus() 
    {
        return autoStatus;
    }
    public void setCallRecords(String callRecords) 
    {
        this.callRecords = callRecords;
    }

    public String getCallRecords() 
    {
        return callRecords;
    }
    public void setGmtCreated(Date gmtCreated) 
    {
        this.gmtCreated = gmtCreated;
    }

    public Date getGmtCreated() 
    {
        return gmtCreated;
    }
    public void setGmtModified(Date gmtModified) 
    {
        this.gmtModified = gmtModified;
    }

    public Date getGmtModified() 
    {
        return gmtModified;
    }
    public void setOptStatus(Integer optStatus) 
    {
        this.optStatus = optStatus;
    }

    public Integer getOptStatus() 
    {
        return optStatus;
    }
    public void setAutoAge(Integer autoAge) 
    {
        this.autoAge = autoAge;
    }

    public Integer getAutoAge() 
    {
        return autoAge;
    }
    public void setAutoPriceRange(String autoPriceRange) 
    {
        this.autoPriceRange = autoPriceRange;
    }

    public String getAutoPriceRange() 
    {
        return autoPriceRange;
    }
    public void setAutoType(String autoType) 
    {
        this.autoType = autoType;
    }

    public String getAutoType() 
    {
        return autoType;
    }
    public void setAutoPlateProv(String autoPlateProv) 
    {
        this.autoPlateProv = autoPlateProv;
    }

    public String getAutoPlateProv() 
    {
        return autoPlateProv;
    }
    public void setAutoPlateDist(String autoPlateDist) 
    {
        this.autoPlateDist = autoPlateDist;
    }

    public String getAutoPlateDist() 
    {
        return autoPlateDist;
    }
    public void setAutoSeeCity(String autoSeeCity) 
    {
        this.autoSeeCity = autoSeeCity;
    }

    public String getAutoSeeCity() 
    {
        return autoSeeCity;
    }
    public void setAutoSeeDistrict(String autoSeeDistrict) 
    {
        this.autoSeeDistrict = autoSeeDistrict;
    }

    public String getAutoSeeDistrict() 
    {
        return autoSeeDistrict;
    }
    public void setAutoSeeStreet(String autoSeeStreet) 
    {
        this.autoSeeStreet = autoSeeStreet;
    }

    public String getAutoSeeStreet() 
    {
        return autoSeeStreet;
    }
    public void setAutoPlateTime(String autoPlateTime) 
    {
        this.autoPlateTime = autoPlateTime;
    }

    public String getAutoPlateTime() 
    {
        return autoPlateTime;
    }
    public void setAutoTransferNum(Integer autoTransferNum) 
    {
        this.autoTransferNum = autoTransferNum;
    }

    public Integer getAutoTransferNum() 
    {
        return autoTransferNum;
    }
    public void setAutoExcitStatus(Integer autoExcitStatus) 
    {
        this.autoExcitStatus = autoExcitStatus;
    }

    public Integer getAutoExcitStatus() 
    {
        return autoExcitStatus;
    }
    public void setCusName(String cusName) 
    {
        this.cusName = cusName;
    }

    public String getCusName() 
    {
        return cusName;
    }
    public void setCusSex(String cusSex) 
    {
        this.cusSex = cusSex;
    }

    public String getCusSex() 
    {
        return cusSex;
    }
    public void setAutoMileage(String autoMileage) 
    {
        this.autoMileage = autoMileage;
    }

    public String getAutoMileage() 
    {
        return autoMileage;
    }
    public void setAutoSeeProv(String autoSeeProv) 
    {
        this.autoSeeProv = autoSeeProv;
    }

    public String getAutoSeeProv() 
    {
        return autoSeeProv;
    }
    public void setPushStatus(Integer pushStatus) 
    {
        this.pushStatus = pushStatus;
    }

    public Integer getPushStatus() 
    {
        return pushStatus;
    }
    public void setPushTime(Date pushTime) 
    {
        this.pushTime = pushTime;
    }

    public Date getPushTime() 
    {
        return pushTime;
    }
    public void setPushType(Integer pushType) 
    {
        this.pushType = pushType;
    }

    public Integer getPushType() 
    {
        return pushType;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("virNum", getVirNum())
            .append("userId", getUserId())
            .append("status", getStatus())
            .append("cusPhone", getCusPhone())
            .append("districtName", getDistrictName())
            .append("brandName", getBrandName())
            .append("autoColor", getAutoColor())
            .append("autoStatus", getAutoStatus())
            .append("callRecords", getCallRecords())
            .append("gmtCreated", getGmtCreated())
            .append("gmtModified", getGmtModified())
            .append("optStatus", getOptStatus())
            .append("autoAge", getAutoAge())
            .append("autoPriceRange", getAutoPriceRange())
            .append("autoType", getAutoType())
            .append("autoPlateProv", getAutoPlateProv())
            .append("autoPlateDist", getAutoPlateDist())
            .append("autoSeeCity", getAutoSeeCity())
            .append("autoSeeDistrict", getAutoSeeDistrict())
            .append("autoSeeStreet", getAutoSeeStreet())
            .append("autoPlateTime", getAutoPlateTime())
            .append("autoTransferNum", getAutoTransferNum())
            .append("autoExcitStatus", getAutoExcitStatus())
            .append("cusName", getCusName())
            .append("cusSex", getCusSex())
            .append("autoMileage", getAutoMileage())
            .append("autoSeeProv", getAutoSeeProv())
            .append("pushStatus", getPushStatus())
            .append("pushTime", getPushTime())
            .append("pushType", getPushType())
            .toString();
    }
}
